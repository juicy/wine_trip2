$(document).ready(function() {

  scroll_fix('body');

  $(window).scroll(function() {

    scroll_fix('body');

  });

  $('.popup .box').click(function(a){
    a.stopPropagation();
  });

  $('.popup').click(function(){
    $(this).fadeOut(150);
  });

  $('.popup .close').click(function(){
    $(this).closest('.popup').fadeOut(150);
  });

  $('#popup0_call').click(function(){
    $('#popup0').fadeIn(150);
  });

  $('#popup1_call').click(function(){
    $('#popup1').fadeIn(150);
  });

  $('#popup2_call').click(function(){
    $('#popup2').fadeIn(150);
  });

  $('#popup3_call').click(function(){
    $('#popup3').fadeIn(150);
  });

  $('#popup4_call').click(function(){
    $('#popup4').fadeIn(150);
  });

  $('#popup5_call').click(function(){
    $('#popup5').fadeIn(150);
  });

  $('#popup6_call').click(function(){
    $('#popup6').fadeIn(150);
  });

  $('.popup_call').click(function(){
    return false;
  });


  $('.fancybox-thumbs').fancybox( {prevEffect : 'none', nextEffect : 'none', closeBtn  : false, arrows    : true, nextClick : true, helpers : {thumbs : {width  : 50,height : 50}}});
  $('.fancybox-thumbs1').fancybox({prevEffect : 'none', nextEffect : 'none', closeBtn  : false, arrows    : true, nextClick : true, helpers : {thumbs : {width  : 50,height : 50}}});
  $('.fancybox-thumbs2').fancybox({prevEffect : 'none', nextEffect : 'none', closeBtn  : false, arrows    : true, nextClick : true, helpers : {thumbs : {width  : 50,height : 50}}});
  $('.fancybox-thumbs3').fancybox({prevEffect : 'none', nextEffect : 'none', closeBtn  : false, arrows    : true, nextClick : true, helpers : {thumbs : {width  : 50,height : 50}}});


});

/* Functions */

function scroll_fix (box) {
  m = $(window).scrollTop() / 1.5;
  v_h = $(window).height();
  if (m > 1556 - v_h) {
    m = 1556 - v_h;
  };
  scroll = '50% ' + m * -1 + 'px';
  $(box).css('backgroundPosition', scroll);
}